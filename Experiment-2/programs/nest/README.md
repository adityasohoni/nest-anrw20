## Instructions to run

Ensure that `nest` is installed in your system.

To run the program, simply run
```
$ sudo -E python gfc.py
```