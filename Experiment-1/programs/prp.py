########################
# SHOULD BE RUN AS ROOT#
########################
from itertools import product

from nest.topology import *
from nest.experiment import *

####################################
# Topology
#
# client -->-- router -->-- server
#
####################################

# Setup test parameters 

n_flows = [4]

bandwidths = [
    ['1gbit', '100mbit']
]

latencies = [
    ['5ms', '10ms']
]

qdiscs = [
    ['pfifo', {'limit': '1000'}],
    ['codel', {}]
]

# Iterate over each test configuration

tests = product(n_flows, bandwidths, latencies, qdiscs)
for n_flow, bandwidth, latency, qdisc in tests:

    # Create server, router and connect
    server = Node('server')
    router = Node('router')
    client = Node('client')
    router.enable_ip_forwarding()

    # Connect client--router--server
    (client_router, router_client) = connect(client, router)
    (router_server, server_router) = connect(router, server)

    # Assign addresses
    client_router.set_address('10.1.1.1/24')
    router_client.set_address('10.1.1.2/24')
    router_server.set_address('10.1.2.2/24')
    server_router.set_address('10.1.2.1/24')

    # Add routes    
    client.add_route('DEFAULT', client_router)
    server.add_route('DEFAULT', server_router)

    # Set client--router link attributes
    client_router.set_attributes(bandwidth[0], latency[0])
    router_client.set_attributes(bandwidth[0], latency[0])
    
    # Set router--server bottleneck link attributes
    router_server.set_attributes(bandwidth[1], latency[1], qdisc[0], **qdisc[1])
    server_router.set_attributes(bandwidth[1], latency[1])

    # Define flow
    flow = Flow(client, server, server_router.get_address(), 0, 60, n_flow)

    # Setup experiment
    exp_name = f'tcp_up-cr_{bandwidth[0]}_{latency[0]}-rs_{bandwidth[1]}_{latency[1]}-{qdisc[0]}-{n_flow}_flows'
    exp = Experiment(exp_name)
    exp.add_tcp_flow(flow)
    exp.require_qdisc_stats(router_server)
    exp.require_node_stats(client)

    # Run experiment
    exp.run()

    print()
